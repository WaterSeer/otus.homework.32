﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID
{
    class DataConsole:IData
    {
        public void Print(string text)
        {
            Console.WriteLine(text);
        }

        public virtual void Print(int number)
        {
            Console.WriteLine(number);
        }

        public int Read()
        {
            return int.Parse(Console.ReadLine());
        }        
    }
}
