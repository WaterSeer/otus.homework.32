﻿using _32_SOLID.IO;

namespace _32_SOLID
{
    interface IData : IDataRead, IDataPrint
    {
        int Input(string text)
        {
            Print(text);
            return Read();
        }

    }
}
