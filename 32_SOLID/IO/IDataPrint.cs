﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID.IO
{
    interface IDataPrint
    {
        void Print(string text);

        void Print(int number);        
    }
}
