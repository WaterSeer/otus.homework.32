﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID.IO
{
    class DataStar:DataConsole
    {
        public override void Print(int number)
        {
            for (int i = 0; i < number; i++)
            {
                Console.WriteLine("*");
            }
            Console.WriteLine();
            
        }
    }
}
