﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID.IO
{
    interface IDataRead
    {
        int Read();
    }
}
