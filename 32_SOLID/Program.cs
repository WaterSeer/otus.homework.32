﻿
using _32_SOLID.IO;

using System;

namespace _32_SOLID
{
    class Program
    {
        static void Main(string[] args)
        {

            DataConsole data = new DataConsole();
            IDataPrint dataPrint = data;
            
            ICalculate calculate1 = new CalculateSum();
            Model model = new Model(data, calculate1);
            model.Execute();

            Console.ReadKey();
        }
    }
}
