﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID.model
{
    class CalculateMul : ICalculate
    {
        public int Calc(int a, int b)
        {
            return a * b;
        }
    }
}
