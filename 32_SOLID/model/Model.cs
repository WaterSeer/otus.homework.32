﻿ using _32_SOLID.IO;
using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID
{
    class Model
    {
        IData data;
        ICalculate calculate;
        int a, b, sum;

        public Model(IData data, ICalculate calculate)
        {
            this.data = data;            
            this.calculate = calculate;
        }

        public void Execute()
        {
            int a = data.Input("Enter a: ");
            int b = data.Input("Enter b: ");
            int sum = calculate.Calc(a, b);
            data.Print("The sum: ");
            data.Print(sum);
        }
        //void Init()
        //{
            
        //    b = data.Input("Enter b: ");
        //}

        //void Calc()
        //{
        //    sum = calculate.Calc(a, b);
        //}

        //void Done()
        //{
        //    data.Print("The sum: ");
        //    data.Print(sum);
        //}
    }
}
