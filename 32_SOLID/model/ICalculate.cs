﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID
{
    interface ICalculate
    {
        int Calc(int a, int b);
    }
}
