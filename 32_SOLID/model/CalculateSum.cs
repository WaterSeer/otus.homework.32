﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _32_SOLID
{
    class CalculateSum : ICalculate
    {
        public int Calc(int a, int b)
        {
            return a + b;
        }
    }
}
